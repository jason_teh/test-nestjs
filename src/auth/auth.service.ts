import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserResponseObject } from 'src/users/users.serializer';
import { UsersService } from 'src/users/users.service';
import { LoginDto } from './dto/login.dto';
import { SignUpDto } from './dto/sign-up.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  login(user) {
    const payload = { email: user.email, sub: user.uuid };

    return `Bearer ${this.jwtService.sign(payload)}`;
    // return {
    //   ...new UserResponseObject(user),
    //   access_token: this.jwtService.sign(payload),
    // };
  }

  signUp(signUpDto: SignUpDto) {
    console.log(signUpDto);
    return 'successfully Sign up';
  }

  async validateUser(email: string, password: string) {
    const user = await this.usersService.findUserByEmail(email);
    if (user && user.password === password) {
      return new UserResponseObject(user);
    }
    return null;
  }
}
