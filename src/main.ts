import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as session from 'express-session';
import * as passport from 'passport';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      // whitelist: true,
    }),
  );

  app.setGlobalPrefix('api');

  app.use(
    session({
      name: 'TODO Session',
      secret: 'NestJsTheBest1sIt?',
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 6000000,
      },
    }),
  );

  app.use(passport.initialize());
  app.use(passport.session());

  const config = new DocumentBuilder()
    .setTitle('TODO')
    .setDescription('TODO API')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  const options = {
    customCss: `
      .topbar-wrapper img {content:''; width:300px; height:auto;}
      .swagger-ui .topbar { background-color: blue; }

      .topbar-wrapper img[alt="Swagger UI"], .topbar-wrapper span {
          visibility: hidden;
      }
    `,
  };
  SwaggerModule.setup('api-doc', app, document, options);

  await app.listen(5002);
}
bootstrap();
