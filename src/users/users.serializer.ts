import { Exclude, Expose } from 'class-transformer';
import { User } from './entities/user.entity';

export interface UserInterface {
  id: number;
  uuid: string;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
}

export class UserResponseObject {
  @Expose()
  uuid: string;
  @Expose()
  first_name: string;
  @Expose()
  last_name: string;
  @Expose()
  email: string;

  @Exclude()
  password: string;
  @Exclude()
  created_at: Date;
  @Exclude()
  updated_at: Date;

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}
